	<div class="top">
					<div class="container">						
							<div class="col-md-9 top-left">
								<ul>
									<li><i class="fa fa-map-marker" aria-hidden="true"></i> 1143 New York, USA</li>
									<li><i class="fa fa-phone" aria-hidden="true"></i> +(010) 221 918 811</li>
									<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">info@example.com</a></li>
								</ul>
							</div>
							<div class="col-md-3 top-middle">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>							
							<div class="clearfix"></div>						
					</div>
				</div>
		<!--top-bar-w3layouts-->
		<div class="top-bar-w3layouts">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a href="index.html">Orion Edu</a></h1>
					</div>
					<!-- navbar-header -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

						<ul class="nav navbar-nav navbar-right">
							<li><a href="/" class="active">Home</a></li>
							<li><a href="/about">About</a></li>
							<li><a href="gallery.html">Gallery</a></li>
							<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category <span class="caret"></span></a>
										<ul class="dropdown-menu">
										 @foreach ($categories as $category)   
											<li><a href="{{ route('FrontEnd.detail',$category->category_name) }}"><?php echo $category->category_name; ?></a></li>											
										@endforeach
										</ul>
							</li> 
							<li><a href="contact.html">Contact</a></li>
						</ul>
					</div>
					<div class="search-bar-agileits">
						<div class="cd-main-header">
							<ul class="cd-header-buttons">
								<li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
							</ul>
							<!-- cd-header-buttons -->
						</div>
						<div id="cd-search" class="cd-search">
							<form action="/search" method="post">
							{{ csrf_field() }}
								<input name="Search_text" type="search" placeholder="Click enter after typing...">
							</form>
						</div>
					</div>
					<div class="clearfix"> </div>
				</nav>
			</div>
		</div>
		<!--//top-bar-w3layouts-->
		<!--Slider-->
		
		<!--//Slider-->