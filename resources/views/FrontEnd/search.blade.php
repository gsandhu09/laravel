@extends('layouts.frontinner')
@section('content')
	<div class="about-1">
	<div class="container">
	<h3 class="title-txt"><span>S</span>earch Result!</h3>
		<div class="ab-agile">
			<div class="col-md-6 aboutleft1">
			<div class="container">
            @if(!empty($result))
            @foreach ($result as $result)  
                       <h1> <?php echo $result->category_name; ?></h1>
                       <h5> <?php echo $result->subcat; ?></h5>
                       <p> <?php echo $result->descr; ?>....<a href="{{ route('FrontEnd.subdetail',[$result->category_name,$result->subcat]) }}">Read More</a></p>
                       <hr />
                       @endforeach
                       @else
                    <?php echo"<h2>Please enter valuable text</h2>";?>
                     @endif
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection