@extends('layouts.frontinner')
@section('content')
<div class="banner"></div>
<div class="about-1">
	<div class="container">
	<h3 class="title-txt"></h3>
		<div class="ab-agile">
			<div class="col-md-8 aboutleft1">				
                @foreach ($cat as $cat)
                    <h3> {{ $cat->category_name }}  </h3>
			    	<p class="para1"> {!!$cat->descr!!}</p>
                 @endforeach 
        	</div>
			<div class="col-md-4 aboutright1">
            
				<ul>
                @foreach ($subcategory as $subcategory)   
                    <li style="list-style-type: none;background-color:#93c83f; padding:10px; margin:5px; border-radius:20px; border:2px solid #1e3953;">
                       <a href="{{ route('FrontEnd.subdetail',[$subcategory->category_name,$subcategory->subcat]) }}" style="color:#1e3953;"> <?php echo $subcategory->subcat; ?></a>
                    </li>
                   @endforeach
                    </ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

@endSection