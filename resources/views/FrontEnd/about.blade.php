@extends('layouts.frontinner')
@section('content')
	<div class="about-1">
	<div class="container">
	<h3 class="title-txt"><span>A</span>bout Us</h3>
		<div class="ab-agile">
			<div class="col-md-6 aboutleft1">
				<h3>Welcome to Education</h3>
				<p class="para1">Donec bibendum velit quis diam venenatis, vulputate aliquam sapien blandit. 
				Etiam dui massa, vehicula a convallis a, facilisis vitae neque.Pellentesque sit amet 
				odio quis libero eleifend congue at ac justo. Suspendisse posuere congue accumsan Vulputate aliquam sapien. </p>
				<p><i class="fa fa-check" aria-hidden="true"></i> Proin tempor pulvinar Vivamus nisi hendrerit et. </p>
				<p><i class="fa fa-check" aria-hidden="true"></i> Proin tempor pulvinar Vivamus nisi hendrerit et. </p>
				<p><i class="fa fa-check" aria-hidden="true"></i> Proin tempor pulvinar Vivamus nisi hendrerit et. </p>
				<p><i class="fa fa-check" aria-hidden="true"></i> Proin tempor pulvinar Vivamus nisi hendrerit et. </p>
			</div>
			<div class="col-md-6 aboutright1">
				<img src="{{ URL::asset('FrontEnd/images/1.jpg') }}" class="img-responsive" alt="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection