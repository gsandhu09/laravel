<div class="sidebar" data-color="orange">
     
     <div class="logo">
       <a href="http://www.creative-tim.com" class="simple-text logo-mini">
         OE
       </a>
       <a href="http://www.creative-tim.com" class="simple-text logo-normal">
         Orion Edu
       </a>
     </div>
     <div class="sidebar-wrapper" id="sidebar-wrapper">
       <ul class="nav">
         <li class="active ">
           <a href="/admin/dashboard/">
             <i class="now-ui-icons design_app"></i>
             <p>Dashboard</p>
           </a>
         </li>
         <li>
           <a href="/admin/category/">
             <i class="now-ui-icons education_atom"></i>
             <p>Manage Categories</p>
           </a>
         </li>
         <li>
           <a href="/admin/subcategory/">
             <i class="now-ui-icons education_atom"></i>
             <p>Manage Sub Categories</p>
           </a>
         </li>
         <li>
           <a href="/admin/imageupload/">
             <i class="now-ui-icons education_atom"></i>
             <p>Manage Images</p>
           </a>
         </li>
        <!-- <li>
           <a href="./map.html">
             <i class="now-ui-icons location_map-big"></i>
             <p>Maps</p>
           </a>
         </li>
         <li>
           <a href="./notifications.html">
             <i class="now-ui-icons ui-1_bell-53"></i>
             <p>Notifications</p>
           </a>
         </li>
         <li>
           <a href="./user.html">
             <i class="now-ui-icons users_single-02"></i>
             <p>User Profile</p>
           </a>
         </li>
         <li>
           <a href="./tables.html">
             <i class="now-ui-icons design_bullet-list-67"></i>
             <p>Table List</p>
           </a>
         </li>
         <li>
           <a href="./typography.html">
             <i class="now-ui-icons text_caps-small"></i>
             <p>Typography</p>
           </a>
         </li>
         <li class="active-pro">
           <a href="./upgrade.html">
             <i class="now-ui-icons arrows-1_cloud-download-93"></i>
             <p>Upgrade to PRO</p>
           </a>
         </li>-->
       </ul>
     </div>
   </div>