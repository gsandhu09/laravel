<footer class="footer">
        <div class="container-fluid">
          <nav>
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Orion Edu
                </a>
              </li>
             
            </ul>
          </nav>
          <div class="copyright" id="copyright">
            &copy;
            <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by
          
            <a href="https://www.orionesolutions.com" target="_blank">By Orion eSolutions</a>.
          </div>
        </div>
      </footer>