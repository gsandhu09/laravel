@extends('layouts.backend')
@section('content')
<div class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Manage Sub Category</h4>
                @if(session()->get('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}  
                  </div>
                @endif  
               <span style="float:right"><a href="/admin/subcategory/create/" class="btn btn-primary"> Add Sub Category</a></span>
              </div>
             
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <tr>
                      <th>
                        Category
                      </th>
                      <th>
                        Sub Category
                      </th>
                      <th>
                        Description
                      </th>
                     <th> Action</th>
                    </tr></thead>
                    @foreach($subcategories as $subcategories)
                    <tbody>
                      <tr>
                        <td>
                        {{$subcategories->category_name}}
                        </td>
                        <td>
                        {{$subcategories->subcat}}
                        </td>
                        <td>
                        {!!$subcategories->descr!!}
                        </td>
                       <td>  <a href="{{ route('subcategory.edit',$subcategories->id)}}" class="btn btn-primary">Edit</a>
                       <a href="{{ route('subcategory.destroy',$subcategories->id)}}" class="btn btn-primary">Delete</a></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection