@extends('layouts.backend')
@section('content')

<div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Add Sub Category</h5>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
              </div>
              <div class="card-body">
              <form method="post" action="{{ route('subcategory.store') }}">
              @csrf
                  <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Category Name</label>
                        <select name="category_name"  class="form-control">
                          @foreach($categories as $categories)
                            <option value="{{ $categories->category_name }}"> {{ $categories->category_name }} </option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Sub Category Name</label>
                        <input type="text" class="form-control" name="subcat" >
                      </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea  class="form-control" id="editor1"  name="descr" style="background-color: transparent; border: 1px solid #E3E3E3; border-radius: 30px;" ></textarea>
                        <script>
                          CKEDITOR.replace( 'editor1' );
                      </script>
                        </div>
                    </div>
                    </div>

                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                       
                        <input type="submit" name="submit" value="Add Sub Category" class="btn btn-primary">
                      </div>
                    </div>
                  </div>                         
                
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
          <div class="card card-user">
              <div style="padding:20px">
                <img src="{{ URL::asset('BackEnd/img/tech.jpg') }}" alt="...">
              </div>
          </div>
      </div>


@endsection