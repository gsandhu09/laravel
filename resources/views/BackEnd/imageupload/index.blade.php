@extends('layouts.backend')
@section('content')
<div class="content">
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Manage Images</h4>
               <span style="float:right"><a href="imageupload/create/" class="btn btn-primary"> Add Image</a></span>
              </div>
             
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <tr>
                      <th>
                        Name
                      </th>
                      <th>
                       Image
                      </th>
                     <th> Action</th>
                    </tr></thead>
                    <tbody>
                    @foreach($image as $image)	
                      <tr>
                        <td>
                        {{$image->name}}
                        </td>
                        <td>
                        <img src="{{ asset('images/'.$image->image) }}" height="100" width="100" >
                        </td>
                       <td><a href="{{ URL('/admin/imageupload/edit/'.$image->id )}}" class="btn btn-primary"> Edit</a>
                         <a href="{{ URL('/admin/imageupload/delete/'.$image->id )}}" class="btn btn-primary"> Delete</a></td>
                      </tr>
                      @endforeach
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

@endsection