@extends('layouts.backend')
@section('content')

<div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Update Record</h5>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
              </div>
              <div class="card-body">
                <form method="post" action="{{ route('imageupload.update', $image->id) }}" enctype='multipart/form-data'>
                  @csrf
                  <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label> Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $image->name }}">
                      </div>
                    </div>
                  </div>
                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                        <label>Image</label>
                        <img src="{{ asset('images/'.$image->image) }}" height="100" width="100" >
                        <input type="file" class="form-control" name="image" >
                      </div>
                    </div>
                    </div>
                  
                    <div class="row">
                    <div class="col-md-8 pr-1">
                      <div class="form-group">
                       
                        <input type="submit" name="submit" value="Update" class="btn btn-primary">
                      </div>
                    </div>
                  </div>                         
                
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
          <div class="card card-user">
              <div style="padding:20px">
                <img src="{{ URL::asset('BackEnd/img/tech.jpg') }}" alt="...">
              </div>
          </div>
      </div>


@endsection