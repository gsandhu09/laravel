<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BackEnd\CategoryController;
use App\Http\Controllers\BackEnd\SubCategoryController;
use App\Category;
use DB;
use App\SubCategory;
use Illuminate\Support\Facades\Input;
use View;
class FrontController extends Controller
{

    public function __construct() 
    {
        // Fetch the data object
        $controller = new CategoryController;
        $categories= $controller->getCategories();
        View::share('categories', $categories);
    }


   public function getHome(){     
        return view('FrontEnd.home');
    }
  public function about(){     
      return view('FrontEnd.about');
    }

  public function display($category_name){
   
        // $user = DB::table('sub_categories')->find($category_name);
        // $subcategory = DB::table('sub_categories')->where('category_name','JAVA');
         $subcategory =DB::select("select * from sub_categories where category_name='$category_name'");     
        $cat  = DB::select("select * from categories where category_name='$category_name'");       
        return view('FrontEnd.details')->with('subcategory',$subcategory)->with('cat',$cat);
    }
  
  public function subdisplay($category_name,$subcat){
    
      $subcategory =DB::select("select * from sub_categories where category_name='$category_name'");
      $subcat =DB::select("select * from sub_categories where category_name='$category_name' and subcat='$subcat'");
      return view('FrontEnd.subdetail')->with('subcategory',$subcategory)->with('subcat',$subcat);
    }
  public function search(){  

    $q = Input::get ( 'Search_text' );  
    if(empty($q)){
        return view ('FrontEnd.search');
    }else{
            $result = SubCategory::where('category_name','LIKE','%'.$q.'%')->orWhere('subcat','LIKE','%'.$q.'%')->get();
            //dd($result);
            if(count($result) > 0)
                return view('FrontEnd.search')->with('result',$result)->withQuery ( $q );
            else return view ('FrontEnd.search')->with('sucsess','No Details found. Try to search again !');
        }
    }




}
