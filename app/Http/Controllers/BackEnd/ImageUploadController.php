<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ImageUpload;

class ImageUploadController extends Controller
{
    public function index(){
        $image = ImageUpload :: orderBy('id')->get();
		
        return view('BackEnd.imageupload.index',['image' => $image]);
    }
    public function store(Request $request)
    {
    //dd($request);
        $request->validate([
            'name'=>'required',
            'image'=>'required|image' //validation for image         
           
        ]);
       
        $img = $request->file('image');
        $newname= $img->getClientOriginalName();   //get image name
        
        $img->move(public_path('images'),$newname);
       							//move image to images folder
            $cat = new ImageUpload([     //array created use any name
            'name' => $request->get('name'),
             'image' => $newname         
        ]);

       //dd($cat);
        $cat->save();
        return redirect('/admin/imageupload/')->with('success', 'image saved!');
    }

 
    public function create(){
    
     return view('BackEnd.imageupload.create');
     }
 
     public function edit($id)
     {
         $image = ImageUpload::find($id);
         return view('BackEnd.imageupload.edit', compact('image'));        
     }

     public function update(Request $request, $id)
    {
        $image = ImageUpload::find($id);
        $old=$image->image;
        $request->validate([
            'name'=>'required'
          
            
        ]);
        $imagedata = ImageUpload::find($id);
        $imagedata->name =  $request->get('name');
             
        
               if ($request->hasfile('image')) {  //if file reuploaded
            $img = $request->file('image');
            $newname= $img->getClientOriginalName();
            $img->move(public_path('images'), $newname);
            $imagedata->image = $newname;
            $imagedata->save();
        }
        else{		//if u do not  want update file save previous image
            $imagedata->save();
        }
      return redirect('/admin/imageupload/')->with('success', 'image updated!');
    }

    public function delete($id)
    {
        $image = ImageUpload::find($id);
        $image->delete();

        return redirect('/admin/imageupload/')->with('success', 'data deleted!');
    }



}
