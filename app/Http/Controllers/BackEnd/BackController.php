<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class BackController extends Controller
{
   public function index(){
         return view('auth.login');
   }
    public function dashboard(){

        if (Auth::check())
        {
             return view('BackEnd.dashboard');
        }  else{
            return view('auth.login');
        }     
    }
}
