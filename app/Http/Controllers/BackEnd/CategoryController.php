<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
class CategoryController extends Controller
{

   public function index(){   
         $categories = Category::all();
         return view('BackEnd.category.index', compact('categories'));
   }
   public function store(Request $request)
   {
      //$name = $request->input('category_name');
     // dd($request);
       $request->validate([
           'category_name'=>'required',
           'descr'=>'required'
       ]);

       $category = new Category([
           'category_name' => $request->get('category_name'),
           'descr' => $request->get('descr')
       ]);
      // dd($category);
       $category->save();
       return redirect('/admin/category')->with('success', 'Category saved!');
   }

   public function getCategories()
   {
       $categories = Category::all();
       return $categories;
   }

   public function create(){   
        return view('BackEnd.category.create');
    }

    public function edit($id)
    {
        $category = Category::find($id);
      //  dd($category);
        return view('BackEnd.category.edit', compact('category'));        
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category_name'=>'required',
            'descr'=>'required'
        ]);
        $category = Category::find($id);
        $category->category_name =  $request->get('category_name');
        $category->descr = $request->get('descr');
        $category->save();
        //dd($category);
        return redirect('/admin/category')->with('success', 'Category updated!');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('/admin/category')->with('success', 'Category deleted!');
    }
}
