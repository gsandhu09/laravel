<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
class SubCategoryController extends Controller
{
    public function index(){
        $subcategories = SubCategory::all();
        return view('BackEnd.subcategory.index', compact('subcategories'));
    }
 
 
    public function create(){
        $controller = new CategoryController;
        $categories= $controller->getCategories();
       
     return view('BackEnd.subcategory.create',compact('categories'));
     }
     public function store(Request $request)
     {
        //$subcat = $request->input('subcat');
         $request->validate([
             'category_name'=>'required',
             'subcat'=>'required',
             'descr'=>'required'
         ]);
 
         $subcat = new SubCategory([
             'category_name' => $request->get('category_name'),
             'subcat' => $request->get('subcat'),
             'descr' => $request->get('descr')
         ]);
         $subcat->save();
         return redirect('/admin/subcategory')->with('success', 'Sub Category saved!');
     }
 
     public function edit($id)
     {
         $subcategory = SubCategory::find($id);
         return view('BackEnd.subcategory.edit', compact('subcategory'));        
     }
     public function update(Request $request, $id)
    {
        $request->validate([
            'category_name'=>'required',
            'subcat'=>'required',
            'descr'=>'required'
        ]);

        $subcategory = SubCategory::find($id);
        $subcategory->category_name =  $request->get('category_name');
        $subcategory->subcat =  $request->get('subcat');
        $subcategory->descr = $request->get('descr');
        $subcategory->save();

        return redirect('/admin/subcategory')->with('success', 'Sub Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCategory::find($id);
        $subcategory->delete();

        return redirect('/admin/subcategory')->with('success', 'Sub Category deleted!');
    }
}
