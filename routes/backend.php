<?php

    Route::get('/','BackController@index');

    Route::group(['middlewear'=>['auth','BackEnd']],function()   
    {
       Route::get('/dashboard','backController@dashboard');

        // Category routes
       Route::get('/category/','CategoryController@index');
       Route::get('/category/create/','CategoryController@create');
       Route::post('/category/store', [ 'as' => 'category.store', 'uses' => 'CategoryController@store']);    
       Route::get('/category/edit/{id}', [ 'as' => 'category.edit', 'uses' => 'CategoryController@edit']);    
       Route::post('/category/update/{id}', [ 'as' => 'category.update', 'uses' => 'CategoryController@update']);
       Route::get('/category/delete/{id}', [ 'as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);    
      

        // SubCategory routes
        Route::get('/subcategory/','SubCategoryController@index');
        Route::get('/subcategory/create/','SubCategoryController@create');
        Route::post('/subcategory/store', [ 'as' => 'subcategory.store', 'uses' => 'SubCategoryController@store']);    
        Route::get('/subcategory/edit/{id}', [ 'as' => 'subcategory.edit', 'uses' => 'SubCategoryController@edit']);    
       Route::post('/subcategory/update/{id}', [ 'as' => 'subcategory.update', 'uses' => 'SubCategoryController@update']);
       Route::get('/subcategory/delete/{id}', [ 'as' => 'subcategory.destroy', 'uses' => 'SubCategoryController@destroy']);    
      



          // ImageUpload routes
          Route::get('/imageupload/','ImageUploadController@index');
          Route::get('/imageupload/create/','ImageUploadController@create');
          Route::post('/imageupload/store', [ 'as' => 'imageupload.store', 'uses' => 'ImageUploadController@store']);
          Route::get('/imageupload/edit/{id}', 'ImageUploadController@edit')->name('imageupload','edit');
          Route::post('/imageupload/update/{id}', [ 'as' => 'imageupload.update', 'uses' => 'ImageUploadController@update']);
          Route::get('/imageupload/delete/{id}', 'ImageUploadController@delete')->name('imageupload','delete');
   
      
    });
?>