<?php
    Route::get('/','FrontController@getHome');
    Route::get('/about','FrontController@about');
    Route::get('/detail/{category_name}', [ 'as' => 'FrontEnd.detail', 'uses' => 'FrontController@display']);    
    Route::get('/subdetail/{category_name}/{subcat}', [ 'as' => 'FrontEnd.subdetail', 'uses' => 'FrontController@subdisplay']); 
    Route::any('/search/', [ 'as' => 'FrontEnd.search', 'uses' => 'FrontController@search']);         
?>